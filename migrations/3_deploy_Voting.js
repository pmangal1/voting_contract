var Ownable = artifacts.require("Ownable");
var Voting = artifacts.require("Voting");

module.exports = function(deployer) {
  // deployment steps
  deployer.deploy(Ownable);
  deployer.deploy(Voting, 10);

};
