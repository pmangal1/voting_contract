# voting-contract

Smart Contracts for Voring

## Steps for starting the project

##### To install truffle and ganache cli:

```shell
# Install ganache-cli
npm install -g ganache-cli

# Install Truffle
npm install -g truffle
```

##### To install smart contact dependencies:

```shell
# from root dir
npm i
```

#### Compiling the Smart Contracts

```shell
# from root dir
truffle compile
```

#### Deploying Smart Contracts

**Setup Truffle config**


```shell
# run ganache-cli
ganache-cli

# deploying contract on ethereum development node
truffle migrate 

# deploying contract on ropsten network
truffle migrate --network ropsten
```
