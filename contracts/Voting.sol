// SPDX-License-Identifier: MIT
pragma solidity 0.8.3;

import "./ownable.sol";

contract Voting is Ownable{
    event Message(string message);
    event Received(address sender, uint value);
    
    // candidates for election 0, 1, 2, 3
    uint[] candidates = [0, 0, 0, 0];
    
    // duration of election
    uint votingDeadline;
    
    // total number of votes
    uint public totalVotes = 0;
    
    // sample address fopr voters
    address[] voters = [0x0000000000000000000000000000000000000000, 0x0000000000000000000000000000000000000001, 0x0000000000000000000000000000000000000002,
    0x0000000000000000000000000000000000000004, 0x0000000000000000000000000000000000000005, 0x0000000000000000000000000000000000000006, 0x0000000000000000000000000000000000000007,
    0x0000000000000000000000000000000000000008, 0x0000000000000000000000000000000000000009, 0x000000000000000000000000000000000000000A];
    
    // registered voter with voting status
    mapping(address => bool) public registeredVoter;
    
    // constructor which decides duration of election and verify users
    constructor (uint durationInMinutes) {
        votingDeadline = block.timestamp + durationInMinutes * 1 minutes;
        markAddressesVerified();
        emit Message('The voting has begun');
    }
    
    // modifier which checks voting duration    
    modifier isVotingInProgress() {
        require(votingDeadline >= block.timestamp, "Voting Ended");
        _;
    }
    
    // register new voter
    function registerVoter(address _address) external isVotingInProgress onlyOwner{
        emit Message('Voter Registered Successfully');
        voters.push(_address);
        registeredVoter[_address] = false;
    }
    
    /// mark address os user to verified
    function markAddressVerified(address _voterAddress) public isVotingInProgress{
        emit Message('Voters address verified to true');
        registeredVoter[_voterAddress] = true;
    }
    
    // mark multiple address verfied
    function markAddressesVerified() private onlyOwner{
        for(uint i=0; i<voters.length; i++){
            markAddressVerified(voters[i]);
        }
    }
    
    // vote for candidate if voter ioos verified
    function voteForCandidate(uint _candidate) public isVotingInProgress{
        require(registeredVoter[msg.sender] == true, 'Please verify first');
        candidates[_candidate]++;
        totalVotes++;
        registeredVoter[msg.sender] = false;
    }
    
    // winner of election after deadline ended
    function winner() external view returns(int){
        require(votingDeadline < block.timestamp, "Voting in progress");
        require(totalVotes >= 5, 'Voting criteria not met');
        uint minimumRequiredVotes = (totalVotes*60)/100;
        for(uint i=0; i<candidates.length; i++){
            if(candidates[i] >= minimumRequiredVotes){
                return int(i);
            }
        }
        
        return -1;
    }
    
    //default fallback split functon to return any funds sent to the contract
    fallback() external payable {
            require(msg.data.length == 0);
            uint amount = msg.value;
            payable(msg.sender).transfer(amount);
            emit Message("Fallback called");
    }
  
    //defaut receive split function to return any funds sent to the contract
    receive() external payable {
        payable(msg.sender).transfer(msg.value);
        emit Received(msg.sender, msg.value);
    }
}
